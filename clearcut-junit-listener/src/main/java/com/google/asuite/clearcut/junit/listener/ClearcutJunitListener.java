/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.asuite.clearcut.junit.listener;
import com.google.auto.service.AutoService;
import org.junit.runner.notification.RunListener;

@AutoService(RunListener.class)
public class ClearcutJunitListener extends RunListener {

    /**
     * Static since the listener gets rebuilt once per class.
     */
    private static final Client client;

    static {
        String tool = "junit";
        String subtool = "junit";
        boolean disable = true;
        try {

            if (EnvironmentInformation.isSysUIRoboTest()) { //sysui
                subtool += "_sysui";
            } else if (EnvironmentInformation.isGradleTest()) { //intellij
                subtool += "_gradle";
            }
            if (EnvironmentInformation.isDebugging()) { //robolectric
                subtool += "_debug";
            }
            if (EnvironmentInformation.isRoboTest()) { //robolectric
                subtool += "_robo";
            }
            if ((EnvironmentInformation.isGoogleDomain() ||
                    EnvironmentInformation
                            .getGitUserIfGoogleEmail(EnvironmentInformation.getGitEmail())
                            .isPresent())) {
                disable = false;
            }
        } catch (Throwable t) {
            System.out.println("Error configuring clearcut listener:");
            t.printStackTrace();
        }
        client = new Client(tool, subtool);
        if (disable) {
            client.disable();
        }
        client.notifyTradefedStartEvent();
    }
}
